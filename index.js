'use strict';

var cheerio = require('cheerio');
var es = require('event-stream');
var iconv = require('iconv-lite');

module.exports = function (settings) {
	var injectViewBox = function (file, callback) {

		var markup = iconv.decode(file.contents, 'utf-8');

		if (markup.indexOf('�') > -1) {
			markup = iconv.decode(file.contents, 'gbk');
			markup = iconv.encode(markup, 'utf-8');
		}

		var dom = cheerio.load(markup, {
			decodeEntities: false,
			xmlMode: false,
			lowerCaseAttributeNames: false
		});

		dom('svg[data-inject-viewbox="true"]').each(function(idx, el) {
			el = dom(el);
			el.removeAttr('data-inject-viewbox');
			var id = el.find('use').attr('href');
			var symbol = dom(id);


			if(symbol.length === 0) return;

			el.attr('viewBox', symbol.attr('viewBox'));
		});

		file.contents = iconv.encode(dom.html(), 'utf-8');
		return callback(null, file);
	}

	return es.map(injectViewBox);
}
